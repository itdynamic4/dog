package com.dynamic4.dog.utils;

import java.io.File;
import java.io.IOException;
import java.util.EnumMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import android.graphics.Bitmap;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.google.zxing.qrcode.encoder.QRCode;
import java.io.FileOutputStream;

/**
 *
 * @author Dynamic4
 */
public final class DogUtils {

    protected static char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
    public static String DEFAULT_DOWNLOAD_PATH = "/sdcard/Download";

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static Long get4BytesAsLong(byte[] bytes) {
        if (bytes.length < 4) {
            throw new IllegalArgumentException("Bytes must be of at least length 4");
        }
            long result = 0;
            for (int i = 0; i < 4; i++) {
                result <<= 4;
                result |= (bytes[i] & 0xFF);
            }
            return result;

    }

    /**
     * Generates and saves a QRCode to a File from stringToEncode
     *
     * @param stringToEncode String to encode.
     * @param outputFile File to save to
     * @param size QRCode size
     */
    public static void encodeStringToQRCodePNG(String stringToEncode, File outputFile, int size) {
        QRCodeWriter writer = new QRCodeWriter();
        try {
            Map<EncodeHintType, Object> hintMap = new EnumMap<>(EncodeHintType.class);
            hintMap.put(EncodeHintType.CHARACTER_SET, "UTF-8");
            hintMap.put(EncodeHintType.MARGIN, 0);
            hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H);
            QRCode qrCode = writer.encodeQRCode(stringToEncode, BarcodeFormat.QR_CODE, size, size, hintMap);
            FileOutputStream out = null;
            try {
                out = new FileOutputStream(outputFile);
                QRCodeWriter.renderResultToBitmap(qrCode, size, size, 0).compress(Bitmap.CompressFormat.PNG, 100, out);
            } catch (Exception e) {
                Logger.getLogger(DogUtils.class.getName()).log(Level.SEVERE, null, e);
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException e) {
                    Logger.getLogger(DogUtils.class.getName()).log(Level.SEVERE, null, e);
                }
            }
        } catch (WriterException e) {
            Logger.getLogger(DogUtils.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}
