package com.dynamic4.dog.tasks;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.widget.Toast;
import com.dynamic4.dog.MainActivity;
import com.dynamic4.dog.qrstream.QRStream;
import com.dynamic4.dog.qrstream.QRStreamData;
import com.dynamic4.dog.utils.DogUtils;
import com.dynamic4.dog.utils.QRStreamGIFWriter;
import com.google.zxing.WriterException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dynamic4
 */
public class QRStreamWriter extends AsyncTask<Object, Long, String> {

    MainActivity mainActivity = null;
    boolean success = false;

    @Override
    protected String doInBackground(Object... args) {
        mainActivity = (MainActivity) args[0];
        final QRStream qrStream = (QRStream) args[1];
        FileOutputStream outStream = null;
        File outputFile = new File(mainActivity.getDirectory(), DogUtils.bytesToHex(qrStream.getSignature()) + ".gif");
        try {
            outStream = new FileOutputStream(outputFile, false);
            outStream.write(QRStreamGIFWriter.constructGIFHeader(mainActivity.getFrameSize()));
            Map<Long, QRStreamData> frames = qrStream.getDataFrames();
            for (long i = 0; i < frames.size(); i++) {
                publishProgress(i, frames.size() + 0L);
                outStream.write(QRStreamGIFWriter.constructFrameData(
                        frames.get(i).getByteArray(mainActivity.getFrameSize(), mainActivity.getErrorCorrectionLevel()), mainActivity.getFrameSize()));
            }
            outStream.write(QRStreamGIFWriter.constructGIFFooter());
            success = true;
        } catch (IOException | WriterException e) {
            mainActivity.showToast("ERROR: " + e.getMessage(), Toast.LENGTH_LONG);
        } finally {
            if (outStream != null) {
                try {
                    outStream.close();
                } catch (IOException ex) {
                }
            }
        }
        return outputFile.getAbsolutePath();
    }

    @Override
    protected void onProgressUpdate(Long... voids) {
        mainActivity.setProgressText("Written " + voids[0] + " of " + voids[1] + "...");
    }

    @Override
    protected void onPostExecute(String result) {
        if (mainActivity != null && success) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.parse("file://" + result), "image/*");
            mainActivity.startActivity(intent);
        }
        try {
            mainActivity.cancelUploadMode();
        } catch (IOException ex) {
            Logger.getLogger(QRStreamWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
