package com.dynamic4.dog.tasks;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.webkit.MimeTypeMap;

import com.dynamic4.dog.qrstream.QRStream;
import com.dynamic4.dog.qrstream.QRStreamException;

import java.io.IOException;
import java.net.URLConnection;

/**
 *
 * @author Dynamic4
 */
public class QRStreamFileWriter extends AsyncTask<Object, Integer, String> {
    private static final String TAG = QRStreamFileWriter.class.getSimpleName();
    Context context = null;
    @Override
    protected String doInBackground(Object... args) {
        String directory = (String) args[0];
        final QRStream qrStream = (QRStream) args[1];
        context = (Context) args[2];
        String absolutePath;
        try {
            Log.i(TAG, "context=" + context.getClass().toString());
            absolutePath = qrStream.writeToFile(directory);
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
            return "ERROR: Could not write to file!";
        } catch (QRStreamException e) {
            Log.e(TAG, e.getMessage());
            return "ERROR: Could not write to file!";
        }
        return absolutePath;
    }

    @Override
    protected void onPostExecute(String result) {
        if (!result.startsWith("ERROR")) {
            if (context != null) {
                String url = "file://" + result;
                Uri fileURI = Uri.parse(url);
                String mime = URLConnection.guessContentTypeFromName(url);
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setDataAndType(fileURI, mime);
                context.startActivity(intent);
            }
        }
    }
}
