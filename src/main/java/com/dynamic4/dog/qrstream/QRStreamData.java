package com.dynamic4.dog.qrstream;

import android.graphics.Bitmap;
import android.util.Base64;

import com.dynamic4.dog.utils.DogUtils;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import java.util.Arrays;
import java.util.EnumMap;
import java.util.Map;

/**
 * @author Dynamic4
 */
public class QRStreamData implements Comparable {

    QRStreamDataHeader header;
    byte[] data;

    public QRStreamData(QRStreamDataHeader header, byte[] data) {
        this.header = header;
        this.data = data;
    }

    @Override
    public int compareTo(Object o) {
        QRStreamData that = (QRStreamData) o;
        if (this.getHeader().getSequenceAsLong() == that.getHeader().getSequenceAsLong())
            return 0;
        return this.getHeader().getSequenceAsLong() < that.getHeader().getSequenceAsLong() ? -1 : 1;
    }

    /**
     * Construct QRStreamDataHeader and QRStreamData from raw QRStreamData bytes
     *
     * @param qrStreamData
     * @throws QRStreamException
     */
    public QRStreamData(final byte[] qrStreamData) throws QRStreamException {
        if (qrStreamData.length < QRStream.HEADER_SIZE) {
            throw new QRStreamException("Malformed QRStream");
        }
        this.header = new QRStreamDataHeader(qrStreamData);
        data = new byte[qrStreamData.length - QRStream.HEADER_SIZE];
        System.arraycopy(qrStreamData, QRStream.HEADER_SIZE,
                data, 0, qrStreamData.length - QRStream.HEADER_SIZE);
    }

    public QRStreamDataHeader getHeader() {
        return header;
    }

    public BitMatrix getBitMatrix(int size, ErrorCorrectionLevel errorCorrection)
            throws WriterException {
        QRCodeWriter writer = new QRCodeWriter();
        return writer.encode(
                Base64.encodeToString(toBytes(), Base64.DEFAULT),
                BarcodeFormat.QR_CODE, size, size, getHintmap(errorCorrection));
    }

    public byte[] getByteArray(int size, ErrorCorrectionLevel errorCorrection)
            throws WriterException {
        QRCodeWriter writer = new QRCodeWriter();
        return QRCodeWriter.renderResultToGIFImage(
                writer.encodeQRCode(
                        Base64.encodeToString(toBytes(), Base64.DEFAULT),
                        BarcodeFormat.QR_CODE, size, size, getHintmap(errorCorrection)), size, size, 0);
    }

    public Bitmap getBitmap(int size, ErrorCorrectionLevel errorCorrection)
            throws WriterException {
        QRCodeWriter writer = new QRCodeWriter();
        return QRCodeWriter.renderResultToBitmap(
                writer.encodeQRCode(
                        Base64.encodeToString(toBytes(), Base64.DEFAULT),
                        BarcodeFormat.QR_CODE, size, size, getHintmap(errorCorrection)), size, size, 0);
    }

    public byte[] toBytes() {
        final byte[] dataBytes = new byte[QRStream.HEADER_SIZE + data.length];
        final byte[] headerBytes = header.getBytes();
        System.arraycopy(headerBytes, 0, dataBytes, 0, headerBytes.length);
        System.arraycopy(data, 0, dataBytes, headerBytes.length, data.length);
        return dataBytes;
    }

    public byte[] getData() {
        return data;
    }

    private Map<EncodeHintType, Object> getHintmap(ErrorCorrectionLevel errorCorrection) {
        Map<EncodeHintType, Object> hintMap = new EnumMap<>(EncodeHintType.class);
        hintMap.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        hintMap.put(EncodeHintType.MARGIN, 0);
        hintMap.put(EncodeHintType.ERROR_CORRECTION, errorCorrection);
        return hintMap;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QRStreamData that = (QRStreamData) o;
        if (header == null && that.header == null) {
            Arrays.equals(this.getData(), that.getData());
        }
        if (header == null || that.header == null) return false;
        final String thisUnique = this.getHeader().getSequenceAsLong() +
                DogUtils.bytesToHex(this.getHeader().getSignature());
        String thatUnique = that.getHeader().getSequenceAsLong() +
                DogUtils.bytesToHex(that.getHeader().getSignature());
        return thisUnique.equals(thatUnique);
    }

    @Override
    public int hashCode() {
        int result = header != null ? header.hashCode() : 0;
        result = 31 * result + Arrays.hashCode(data);
        return result;
    }
}
