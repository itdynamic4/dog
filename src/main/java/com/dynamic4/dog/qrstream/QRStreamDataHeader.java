package com.dynamic4.dog.qrstream;

import com.dynamic4.dog.utils.DogUtils;

/**
 *
 * @author Dynamic4
 */
public final class QRStreamDataHeader {

    private final byte[] signature;
    private final byte[] sequence = new byte[QRStream.SEQUENCE_SIZE];

    /**
     * Construct header from a QRStreamData bytes
     *
     * @param signature Signature of the QRStreamData
     * @param frameSequence Frame number
     */
    public QRStreamDataHeader(final byte[] signature, final long frameSequence) {
        if (signature == null) {
            throw new IllegalArgumentException("signature cannot be null");
        }
        if (signature.length != QRStream.SIGNATURE_SIZE) {
            throw new IllegalArgumentException("signature must be 4 bytes long");
        }
        if (frameSequence >= 4294967296L || frameSequence < 0) {
            throw new IllegalArgumentException("Frame sequence must be less than 4294967296 and higher than 0");
        }
        sequence[0] = (byte) ((frameSequence >> 24) & 0xFF);
        sequence[1] = (byte) ((frameSequence >> 16) & 0xFF);
        sequence[2] = (byte) ((frameSequence >> 8) & 0xFF);
        sequence[3] = (byte) (frameSequence & 0xFF);
        this.signature = signature;
    }

    /**
     * Construct header from a QRStreamData bytes
     *
     * @param qrStreamData
     * @throws com.dynamic4.dog.qrstream.QRStreamException
     */
    public QRStreamDataHeader(final byte[] qrStreamData) throws QRStreamException {
        if (qrStreamData[0] != QRStream.VERSION || qrStreamData[1] != QRStream.HEADER_PADDING) {
            throw new QRStreamException("Malformed header");
        }
        signature = new byte[32];
        System.arraycopy(qrStreamData, 2, signature, 0, signature.length);
        System.arraycopy(qrStreamData, 34, sequence, 0, sequence.length);
    }

    public byte[] getSignature() {
        return signature;
    }

    public long getSequenceAsLong() {
        return DogUtils.get4BytesAsLong(sequence);
    }

    public byte[] getSequence() {
        return sequence;
    }

    public byte[] getBytes() {
        final byte[] headerBytes = new byte[QRStream.HEADER_SIZE];
        headerBytes[0] = QRStream.VERSION;
        headerBytes[1] = QRStream.HEADER_PADDING;
        System.arraycopy(signature, 0, headerBytes, 2, signature.length);
        System.arraycopy(sequence, 0, headerBytes, 34, sequence.length);
        return headerBytes;
    }

}
