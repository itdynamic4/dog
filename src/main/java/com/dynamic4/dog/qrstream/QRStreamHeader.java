/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dynamic4.dog.qrstream;

import com.dynamic4.dog.utils.DogUtils;

/**
 *
 * @author Dynamic4
 */
public class QRStreamHeader extends QRStreamData {

    private final int frameSize;
    private final String fileName;
    private final int streamSize;

    private static byte[] writeQRStreamHeaderData(final String fileName, final int frameSize, final int streamSize) {
        final byte[] fileNameBytes = fileName.getBytes();
        byte[] frameZeroData = new byte[fileNameBytes.length + 8];
        //Generate frame 0
        frameZeroData[0] = (byte) ((streamSize >> 24) & 0xFF);
        frameZeroData[1] = (byte) ((streamSize >> 16) & 0xFF);
        frameZeroData[2] = (byte) ((streamSize >> 8) & 0xFF);
        frameZeroData[3] = (byte) (streamSize & 0xFF);
        frameZeroData[4] = (byte) ((frameSize >> 24) & 0xFF);
        frameZeroData[5] = (byte) ((frameSize >> 16) & 0xFF);
        frameZeroData[6] = (byte) ((frameSize >> 8) & 0xFF);
        frameZeroData[7] = (byte) (frameSize & 0xFF);
        System.arraycopy(fileNameBytes, 0,
                frameZeroData, 8, fileNameBytes.length);
        return frameZeroData;
    }

    public int getFrameSize() {
        return frameSize;
    }

    public String getFileName() {
        return fileName;
    }

    public int getStreamSize() {
        return streamSize;
    }

    public QRStreamHeader(
            final String fileNameParameter,
            final int frameSizeParameter,
            final int streamSizeParameter,
            final byte[] signatureParameter) {
        super(new QRStreamDataHeader(signatureParameter, 0L),
                writeQRStreamHeaderData(fileNameParameter,
                        frameSizeParameter,
                        streamSizeParameter));
        fileName = fileNameParameter;
        frameSize = frameSizeParameter;
        streamSize = streamSizeParameter;
    }

    @Override
    public final byte[] getData() {
        return super.getData();
    }

    public QRStreamHeader(byte[] qrStreamData) throws QRStreamException {
        super(qrStreamData);
        byte[] dataBytes = getData();
        if (dataBytes.length < 9) {
            throw new QRStreamException("Malformed QRStreamHeader");
        }
        if (this.getHeader().getSequenceAsLong() != 0) {
            throw new QRStreamException("Malformed QRStreamHeader Sequence must be 0");
        }
        final byte[] fileNameBytes = new byte[dataBytes.length - 8];
        byte[] streamSizeBytes = new byte[4];
        byte[] frameSizeSizeBytes = new byte[4];
        System.arraycopy(dataBytes, 0,
                streamSizeBytes, 0, 4);
        System.arraycopy(dataBytes, 4,
                frameSizeSizeBytes, 0, 4);
        System.arraycopy(dataBytes, 8,
                fileNameBytes, 0, fileNameBytes.length);
        fileName = new String(fileNameBytes);
        frameSize = DogUtils.get4BytesAsLong(frameSizeSizeBytes).intValue();
        streamSize = DogUtils.get4BytesAsLong(streamSizeBytes).intValue();
    }
}
