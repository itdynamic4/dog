/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.dynamic4.dog.qrstream;

/**
 * Hello from ITEC 370!
 *
 * @author dynamic4
 */
public class QRStreamException extends Exception {

    public QRStreamException() {
    }

    public QRStreamException(String detailMessage) {
        super(detailMessage);
    }

    public QRStreamException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

}
