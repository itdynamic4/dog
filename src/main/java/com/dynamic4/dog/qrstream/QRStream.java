package com.dynamic4.dog.qrstream;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Dynamic4
 */
public final class QRStream {

    public static final byte VERSION = 1;
    public static final byte HEADER_PADDING = (byte) 0xFF;

    public static String SIGNATURE_DIGEST = "SHA-256";
    public static final int HEADER_SIZE = 38;
    public static final int SIGNATURE_SIZE = 32;
    public static final int SEQUENCE_SIZE = 4;

    //List size being an int is a limitation.
    /**
     * @TODO use a collection which may expand to max 0xFFFFFFFF
     */
    public Map<Long, QRStreamData> dataFrames = null;
    final byte[] signature;
    final String fileName;
    final int frameSize;
    final int streamSize;
    private MessageDigest signatureDigest = null;

    public QRStream(final byte[] fileData, final String fileName, final int frameSize)
            throws NoSuchAlgorithmException {
        this.signatureDigest = MessageDigest.getInstance(SIGNATURE_DIGEST);
        if (fileData == null) {
            throw new IllegalArgumentException("File data cannot be null");
        }
        if (fileName == null || fileName.isEmpty()) {
            throw new IllegalArgumentException("File name cannot be null or empty");
        }
        final byte[] fileNameBytes = fileName.getBytes();
        final byte[] fileWithNameAppended = new byte[fileData.length + fileNameBytes.length];
        System.arraycopy(fileData, 0, fileWithNameAppended, 0, fileData.length);
        System.arraycopy(fileNameBytes, 0, fileWithNameAppended,
                fileData.length, fileNameBytes.length);
        this.fileName = fileName;
        this.signature = signatureDigest.digest(fileWithNameAppended);
        this.frameSize = frameSize;
        generateDataFrames(frameSize, fileData);
        this.streamSize = getDataFrames().size();
    }

    public QRStream(final String fileName, final byte[] fileSignature, final int frameSize,
                    final int streamSize)
            throws NoSuchAlgorithmException {
        this.signatureDigest = MessageDigest.getInstance(SIGNATURE_DIGEST);
        if (fileSignature == null) {
            throw new IllegalArgumentException("File signature cannot be null");
        }
        if (fileName == null || fileName.isEmpty()) {
            throw new IllegalArgumentException("File name cannot be null or empty");
        }
        this.fileName = fileName;
        this.signature = fileSignature;
        this.frameSize = frameSize;
        dataFrames = new ConcurrentHashMap<>();
        dataFrames.put(0L, new QRStreamHeader(fileName, frameSize, streamSize, signature));
        this.streamSize = streamSize;
    }

    public int getStreamSize() {
        return streamSize;
    }

    public int getFrameSize() {
        return frameSize;
    }

    public String getFileName() {
        return fileName;
    }

    public byte[] getSignature() {
        return signature;
    }

    public void addFrame(long sequence, byte[] data) {
        getDataFrames().put(sequence, new QRStreamData(new QRStreamDataHeader(signature, sequence), data));
    }

    public void addFrame(QRStreamData frame) {
        getDataFrames().put(frame.getHeader().getSequenceAsLong(), frame);
    }

    public String writeToFile(String outputDirectory) throws IOException, QRStreamException {
        ByteArrayOutputStream outStream = null;
        File outputFile = new File(outputDirectory, getFileName());
        String absolutePath = outputFile.getAbsolutePath();
        outStream = new ByteArrayOutputStream();
        Map<Long, QRStreamData> sortedFrames = getDataFrames();
        for (long i = 0; i < sortedFrames.size(); i++) {
            QRStreamData fileChunk = sortedFrames.get(i);
            if (fileChunk == null) {
                throw new QRStreamException("Received a null frame.");
            }
            if (fileChunk.getHeader().getSequenceAsLong() != 0) {
                outStream.write(fileChunk.getData());
            }
        }
        final byte[] fileData = outStream.toByteArray();
        outStream.write(getFileName().getBytes());
        if (!Arrays.equals(getSignature(), signatureDigest.digest(outStream.toByteArray()))) {
            throw new QRStreamException("Signature did not match the final downloaded data.");
        }
        FileOutputStream out = new FileOutputStream(outputFile, false);
        out.write(fileData);
        out.close();
        return absolutePath;
    }

    public Map<Long, QRStreamData> getDataFrames() {
        if (dataFrames == null) {
            throw new IllegalStateException("Called getFrames when no frames were generated");
        }
        return dataFrames;
    }

    private void generateDataFrames(final int frameSize, final byte[] fileData) {
        dataFrames = new ConcurrentHashMap<>();
        int bytesEncoded = 0;
        long frameSequence = 0;
        while (bytesEncoded < fileData.length) {
            frameSequence++;
            byte[] dataChunk = new byte[(bytesEncoded + frameSize) > fileData.length ? fileData.length - bytesEncoded : frameSize];
            System.arraycopy(fileData, bytesEncoded,
                    dataChunk, 0, dataChunk.length);
            dataFrames.put(frameSequence, new QRStreamData(new QRStreamDataHeader(signature, frameSequence), dataChunk));
            bytesEncoded += frameSize;
        }
        //Generate frame 0
        dataFrames.put(0L, new QRStreamHeader(fileName, frameSize, dataFrames.size() + 1, signature));
    }

}
