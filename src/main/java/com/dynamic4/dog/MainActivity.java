package com.dynamic4.dog;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.Toast;

import com.dynamic4.dog.intents.BrowseFile;
import com.dynamic4.dog.qrstream.QRStream;
import com.dynamic4.dog.tasks.QRStreamWriter;
import com.dynamic4.dog.utils.DogUtils;
import com.dynamic4.dog.zxing.ReadQRStream;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dynamic4
 */

public class MainActivity extends AppCompatActivity implements OnClickListener {

    private static final int BROWSE_FILE = 1;
    private static final int RECEIVE_FILE = 1;
    private ImageButton sendFileButton;
    private ImageButton recFileButton;
    private File selectedFile;
    private AlertDialog progressDialog;
    private ErrorCorrectionLevel errorCorrectionLevel;
    private int frameSize;
    SharedPreferences  prefs;

    public ErrorCorrectionLevel getErrorCorrectionLevel() {
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String tmp = prefs.getString("error_cor","High");
        switch (tmp){
            case "High":
                errorCorrectionLevel = ErrorCorrectionLevel.H;
                break;
            case "Medium":
                errorCorrectionLevel = ErrorCorrectionLevel.M;
                break;
            case "Low":
                errorCorrectionLevel = ErrorCorrectionLevel.L;
                break;
        }
        return errorCorrectionLevel;
    }

    public int getFrameSize() {
        prefs = PreferenceManager.getDefaultSharedPreferences(this);
        String tmp = prefs.getString("frame_size","512");
        frameSize = Integer.parseInt(tmp.toString());
        return frameSize;
    }

    public String getDirectory(){
        prefs = getSharedPreferences("ChosenDir", Context.MODE_PRIVATE);
        //"directory" is key that links the shared preference from settings
        //"/sdcard/Download/" is the default value.  Set the default directory here
        String tmp = prefs.getString("directory", DogUtils.DEFAULT_DOWNLOAD_PATH);
        return tmp;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sendFileButton = (ImageButton) findViewById(R.id.btnSnd);
        recFileButton = (ImageButton) findViewById(R.id.btnRec);
        sendFileButton.setOnClickListener(this);
        recFileButton.setOnClickListener(this);

    }

    public void showToast(final String message, final int length) {
        this.runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(MainActivity.this, message, length)
                        .show();
            }
        });
    }

    public AlertDialog getProgressDialog() {
        return progressDialog;
    }

    public void enterUploadMode(final int numberOfFramesToWrite) {
        this.runOnUiThread(new Runnable() {
                               public void run() {
                                   progressDialog.show();
                                   progressDialog.setMessage("Preparing QRStream (" +
                                           numberOfFramesToWrite + " Frame Size)...");
                               }
                           }
        );
    }

    public void setProgressText(final String text) {
        this.runOnUiThread(new Runnable() {
                               public void run() {
                                   progressDialog.setMessage(text);
                               }
                           }
        );
    }

    public void cancelUploadMode() throws IOException {
        this.runOnUiThread(new Runnable() {
                               public void run() {
                                   progressDialog.cancel();
                               }
                           }
        );
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSnd:
                startActivityForResult(
                        new Intent(this, BrowseFile.class), BROWSE_FILE);
                break;
            case R.id.btnRec:
                startActivityForResult(
                        new Intent(this, ReadQRStream.class), RECEIVE_FILE);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case BROWSE_FILE:
                    if (data.hasExtra(BrowseFile.EXTRA_FILE_PATH)) {
                        selectedFile = new File(data.getStringExtra(BrowseFile.EXTRA_FILE_PATH));
                        progressDialog = new ProgressDialog.Builder(this)
                                .setMessage("")
                                .setCancelable(false)
                                .create();
                        new AlertDialog.Builder(this)
                                .setTitle("Prepare QRStream")
                                .setMessage("Are you sure (" + selectedFile.getPath()
                                        + ") is the file you'd like to send?")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                byte[] fileData;
                                                try {
                                                    fileData = FileUtils.readFileToByteArray(selectedFile);
                                                    if (fileData != null) {
                                                        enterUploadMode(getFrameSize());
                                                        QRStream qrStream = new QRStream(fileData, selectedFile.getName(), getFrameSize());
                                                        new QRStreamWriter().execute(MainActivity.this, qrStream);
                                                    }
                                                } catch (IOException | NoSuchAlgorithmException ex) {
                                                    Logger.getLogger(MainActivity.class.getName()).log(Level.SEVERE, null, ex);
                                                }
                                            }
                                        }
                                )
                                .
                                        setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                progressDialog.cancel();
                                            }
                                        })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();
                    }
                    break;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this,SettingsActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.action_help){
            Intent intent = new Intent(this,ManualActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
    public void clickSettings(View view){
        Intent intent = new Intent(this,SettingsActivity.class);
        startActivity(intent);
    }

}
