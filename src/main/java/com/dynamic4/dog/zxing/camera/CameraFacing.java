package com.dynamic4.dog.zxing.camera;

/**
 * Adapted from 2012 ZXing (https://github.com/zxing/)
 *
 * @author Dynamic4
 */
public enum CameraFacing {

  BACK,  // must be value 0!
  FRONT, // must be value 1!

}
