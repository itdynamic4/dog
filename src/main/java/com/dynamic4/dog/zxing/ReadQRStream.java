package com.dynamic4.dog.zxing;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.dynamic4.dog.R;
import com.dynamic4.dog.qrstream.QRStream;
import com.dynamic4.dog.qrstream.QRStreamData;
import com.dynamic4.dog.qrstream.QRStreamException;
import com.dynamic4.dog.qrstream.QRStreamHeader;
import com.dynamic4.dog.tasks.QRStreamFileWriter;
import com.dynamic4.dog.utils.DogUtils;
import com.dynamic4.dog.zxing.camera.CameraManager;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.DecodeHintType;
import com.google.zxing.Result;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Adapted from 2012 ZXing (https://github.com/zxing/)
 *
 * @author Dynamic4
 */
public final class ReadQRStream extends Activity implements SurfaceHolder.Callback {

    private static final String TAG = ReadQRStream.class.getSimpleName();
    private CameraManager cameraManager;
    private CaptureActivityHandler handler;
    private Result savedResultToShow;
    private ViewfinderView viewfinderView;
    private TextView statusView;
    private QRStream downloadingQRStream = null;
    private View resultView;
    private boolean hasSurface;
    private AtomicBoolean downloadComplete = new AtomicBoolean(false);
    private InactivityTimer inactivityTimer;
    private BeepManager beepManager;
    private AmbientLightManager ambientLightManager;
    private Collection<BarcodeFormat> decodeFormats;
    private Map<DecodeHintType, ?> decodeHints;
    public static final int REQUEST_CAMERA_PERMISSIONS = 2;
    private String characterSet;

    ViewfinderView getViewfinderView() {
        return viewfinderView;
    }

    public Handler getHandler() {
        return handler;
    }

    CameraManager getCameraManager() {
        return cameraManager;
    }

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(this, Manifest.permission.VIBRATE) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(this, Manifest.permission.FLASHLIGHT) != PackageManager.PERMISSION_GRANTED
                ) {
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.CAMERA,
                    Manifest.permission.INTERNET,
                    Manifest.permission.VIBRATE,
                    Manifest.permission.FLASHLIGHT,
            }, REQUEST_CAMERA_PERMISSIONS);
        }
        Window window = getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.capture);


        hasSurface = false;
        downloadingQRStream = null;
        inactivityTimer = new InactivityTimer(this);
        beepManager = new BeepManager(this);
        ambientLightManager = new AmbientLightManager(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // CameraManager must be initialized here, not in onCreate(). This is necessary because we don't
        // want to open the camera driver and measure the screen size if we're going to show the help on
        // first launch. That led to bugs where the scanning rectangle was the wrong size and partially
        // off screen.
        cameraManager = new CameraManager(getApplication());
        viewfinderView = (ViewfinderView) findViewById(R.id.viewfinder_view);
        viewfinderView.setCameraManager(cameraManager);
        resultView = findViewById(R.id.result_view);
        statusView = (TextView) findViewById(R.id.status_view);
        handler = null;
        setRequestedOrientation(getCurrentOrientation());
        resetStatusView();
        beepManager.updatePrefs();
        downloadComplete.set(false);
        ambientLightManager.start(cameraManager);
        inactivityTimer.onResume();
        SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
        SurfaceHolder surfaceHolder = surfaceView.getHolder();
        if (hasSurface) {
            // The activity was paused but not stopped, so the surface still exists. Therefore
            // surfaceCreated() won't be called, so init the camera here.
            initCamera(surfaceHolder);
        } else {
            // Install the callback and wait for surfaceCreated() to init the camera.
            surfaceHolder.addCallback(this);
        }
    }

    private int getCurrentOrientation() {
        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            switch (rotation) {
                case Surface.ROTATION_0:
                case Surface.ROTATION_90:
                    return ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE;
                default:
                    return ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE;
            }
        } else {
            switch (rotation) {
                case Surface.ROTATION_0:
                case Surface.ROTATION_270:
                    return ActivityInfo.SCREEN_ORIENTATION_PORTRAIT;
                default:
                    return ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT;
            }
        }
    }

    @Override
    protected void onPause() {
        if (handler != null) {
            handler.quitSynchronously();
            handler = null;
        }
        inactivityTimer.onPause();
        ambientLightManager.stop();
        beepManager.close();
        cameraManager.closeDriver();
        //historyManager = null; // Keep for onActivityResult
        if (!hasSurface) {
            SurfaceView surfaceView = (SurfaceView) findViewById(R.id.preview_view);
            SurfaceHolder surfaceHolder = surfaceView.getHolder();
            surfaceHolder.removeCallback(this);
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        inactivityTimer.shutdown();
        super.onDestroy();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_BACK:
                setResult(RESULT_CANCELED);
                finish();
                return true;
            case KeyEvent.KEYCODE_FOCUS:
            case KeyEvent.KEYCODE_CAMERA:
                // Handle these events so they don't launch the Camera app
                return true;
            // Use volume up/down to turn on light
            case KeyEvent.KEYCODE_VOLUME_DOWN:
                cameraManager.setTorch(false);
                return true;
            case KeyEvent.KEYCODE_VOLUME_UP:
                cameraManager.setTorch(true);
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

    }

    private void decodeOrStoreSavedBitmap(Bitmap bitmap, Result result) {
        // Bitmap isn't used yet -- will be used soon
        if (handler == null) {
            savedResultToShow = result;
        } else {
            if (result != null) {
                savedResultToShow = result;
            }
            if (savedResultToShow != null) {
                Message message = Message.obtain(handler, R.id.decode_succeeded, savedResultToShow);
                handler.sendMessage(message);
            }
            savedResultToShow = null;
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (holder == null) {
            Log.e(TAG, "*** WARNING *** surfaceCreated() gave us a null surface!");
        }
        if (!hasSurface) {
            hasSurface = true;
            initCamera(holder);
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        hasSurface = false;
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    /**
     * A valid barcode has been found, so give an indication of success and show
     * the results.
     *
     * @param rawResult The contents of the barcode.
     */
    public void handleDecode(Result rawResult) {
        Log.d(TAG, "MESSAGE RECEIVED!!" + rawResult.getText());
        inactivityTimer.onActivity();
        byte[] decodedQRCode;
        try {
            decodedQRCode = Base64.decode(rawResult.getText(), Base64.DEFAULT);
        } catch (IllegalArgumentException ex) {
            decodedQRCode = new byte[0];
        }
        Log.d(TAG, "Base64 Decoded=" + DogUtils.bytesToHex(decodedQRCode));
        if (decodedQRCode.length > QRStream.HEADER_SIZE) {
            if (!downloadComplete.get()) {
                if (downloadingQRStream == null) {
                    Log.e(TAG, "DOWNLOAD STARTED!!");
                    try {
                        QRStreamHeader possibleQRStreamData;
                        possibleQRStreamData = new QRStreamHeader(decodedQRCode);
                        beepManager.playBeepSoundAndVibrate();
                        resetStatusView();
                        statusView.setText("Downloading QRStream (" + possibleQRStreamData.getFileName() + ") " +
                                possibleQRStreamData.getStreamSize() + " frames.");
                        statusView.setVisibility(View.VISIBLE);
                        Log.d(TAG, DogUtils.bytesToHex(possibleQRStreamData.toBytes()));
                        downloadingQRStream = new QRStream(
                                possibleQRStreamData.getFileName(),
                                possibleQRStreamData.getHeader().getSignature(),
                                possibleQRStreamData.getFrameSize(),
                                possibleQRStreamData.getStreamSize());

                    } catch (QRStreamException ex) {
                        Log.d(TAG, "Read QRCode was not a header");
                    } catch (NoSuchAlgorithmException ex) {
                        throw new IllegalStateException("Could not get file signature algorithm");
                    }
                } else {
                    Log.d(TAG, "DOWNLOAD IN PROGRESS!!");
                    try {
                        QRStreamData possibleQRStreamData;
                        possibleQRStreamData = new QRStreamData(decodedQRCode);
                        Log.i(TAG, DogUtils.bytesToHex(possibleQRStreamData.toBytes()));
                        this.downloadingQRStream.addFrame(possibleQRStreamData);
                        int currentSize = downloadingQRStream.getDataFrames().size();
                        int totalSize = downloadingQRStream.getStreamSize();
                        statusView.setText("Got QRStream (" +
                                this.downloadingQRStream.getFileName() + ") " +
                                currentSize + "/" + totalSize);
                        if (currentSize == totalSize) {
                            downloadComplete.set(true);
                            statusView.setText("Download complete!");
                            beepManager.playBeepSoundAndVibrate();
                            String directory = PreferenceManager.getDefaultSharedPreferences(
                                    this.getBaseContext()).getString("directory", DogUtils.DEFAULT_DOWNLOAD_PATH);
                            new QRStreamFileWriter().execute(directory, this.downloadingQRStream, this.getBaseContext());
                            finish();
                        }
                    } catch (QRStreamException ex) {
                        Log.d(TAG, "Read QRCode was not a data frame");
                    }
                }
            }
        } else {
            Log.d(TAG, "Read incorrect QRStream " +
                    decodedQRCode.length +
                    " must be greater than " +
                    QRStream.HEADER_SIZE);
        }

    }

    private void initCamera(SurfaceHolder surfaceHolder) {
        if (surfaceHolder == null) {
            throw new IllegalStateException("No SurfaceHolder provided");
        }
        if (cameraManager.isOpen()) {
            Log.w(TAG, "initCamera() while already open -- late SurfaceView callback?");
            return;
        }
        try {
            cameraManager.openDriver(surfaceHolder);
            // Creating the handler starts the preview, which can also throw a RuntimeException.
            if (handler == null) {
                handler = new CaptureActivityHandler(this, decodeFormats, decodeHints, characterSet, cameraManager);
            }
            decodeOrStoreSavedBitmap(null, null);
        } catch (IOException ioe) {
            Log.w(TAG, ioe);
            displayFrameworkBugMessageAndExit();
        } catch (RuntimeException e) {
            // Barcode Scanner has seen crashes in the wild of this variety:
            // java.?lang.?RuntimeException: Fail to connect to camera service
            Log.w(TAG, "Unexpected error initializing camera", e);
            displayFrameworkBugMessageAndExit();
        }
    }

    private void displayFrameworkBugMessageAndExit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.app_name));
        builder.setMessage(getString(R.string.msg_camera_framework_bug));
        builder.setPositiveButton(R.string.button_ok, new FinishListener(this));
        builder.setOnCancelListener(new FinishListener(this));
        builder.show();
    }

    public void restartPreviewAfterDelay(long delayMS) {
        if (handler != null) {
            handler.sendEmptyMessageDelayed(R.id.restart_preview, delayMS);
        }
        resetStatusView();
    }

    private void resetStatusView() {
        resultView.setVisibility(View.GONE);
        statusView.setText(R.string.msg_default_status);
        statusView.setVisibility(View.VISIBLE);
        viewfinderView.setVisibility(View.VISIBLE);
    }

    public void drawViewfinder() {
        viewfinderView.drawViewfinder();
    }
}
