package com.dynamic4.dog.zxing;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.DecodeHintType;
import com.google.zxing.ResultPointCallback;
import android.os.Handler;
import android.os.Looper;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

/**
 * Adapted from 2012 ZXing (https://github.com/zxing/)
 *
 * @author Dynamic4
 */
final class DecodeThread extends Thread {

    public static final String BARCODE_BITMAP = "barcode_bitmap";
    public static final String BARCODE_SCALED_FACTOR = "barcode_scaled_factor";

    private final ReadQRStream activity;
    private Handler handler;
    private final CountDownLatch handlerInitLatch;

    DecodeThread(ReadQRStream activity,
            Collection<BarcodeFormat> decodeFormats,
            Map<DecodeHintType, ?> baseHints,
            String characterSet,
            ResultPointCallback resultPointCallback) {

        this.activity = activity;
        handlerInitLatch = new CountDownLatch(1);

        // Log.i("DecodeThread", "Hints: " + hints);
    }

    Handler getHandler() {
        try {
            handlerInitLatch.await();
        } catch (InterruptedException ie) {
            // continue?
        }
        return handler;
    }

    @Override
    public void run() {
        Looper.prepare();
        handler = new DecodeHandler(activity);
        handlerInitLatch.countDown();
        Looper.loop();
    }

}
