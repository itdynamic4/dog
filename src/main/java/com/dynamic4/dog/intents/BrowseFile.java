package com.dynamic4.dog.intents;

import java.io.File;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.Manifest;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.dynamic4.dog.R;


/**
 * @author Dynamic4
 */
public class BrowseFile extends ListActivity {

    public final static String EXTRA_FILE_PATH = "file_path";
    public final static String EXTRA_ACCEPTED_FILE_EXTENSIONS = "accepted_file_extensions";
    public static final int REQUEST_WRITE_AND_READ = 1;
    private final static String DEFAULT_INITIAL_DIRECTORY = "/sdcard/";

    protected File currentDirectory;
    protected ArrayList<File> currentFiles;
    FilePickerListAdapter fileAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                ) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_WRITE_AND_READ);
        }
        LayoutInflater inflator = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View emptyView = inflator.inflate(R.layout.empty_view, null);
        ((ViewGroup) getListView().getParent()).addView(emptyView);
        getListView().setEmptyView(emptyView);
        currentDirectory = new File(DEFAULT_INITIAL_DIRECTORY);
        currentFiles = new ArrayList<File>();
        fileAdapter = new FilePickerListAdapter(this, currentFiles);
        setListAdapter(fileAdapter);
        if (getIntent().hasExtra(EXTRA_FILE_PATH)) {
            currentDirectory = new File(getIntent().getStringExtra(EXTRA_FILE_PATH));
        }
    }

    @Override
    protected void onResume() {
        refreshFilesList();
        super.onResume();
    }

    protected void refreshFilesList() {
        currentFiles.clear();
        File[] files = currentDirectory.listFiles();
        if (files != null && files.length > 0) {
            currentFiles.addAll(Arrays.asList(files));
            Collections.sort(currentFiles, new FileComparator());
        }
        fileAdapter.notifyDataSetChanged();
    }

    @Override
    public void onBackPressed() {
        if (currentDirectory.getParentFile() != null) {
            currentDirectory = currentDirectory.getParentFile();
            refreshFilesList();
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        File newFile = (File) l.getItemAtPosition(position);
        if (newFile.isFile()) {
            Intent extra = new Intent();
            extra.putExtra(EXTRA_FILE_PATH, newFile.getAbsolutePath());
            setResult(RESULT_OK, extra);
            finish();
        } else {
            currentDirectory = newFile;
            refreshFilesList();
        }
        super.onListItemClick(l, v, position, id);
    }

    private class FilePickerListAdapter extends ArrayAdapter<File> {

        final private List<File> files;

        public FilePickerListAdapter(Context context, List<File> objects) {
            super(context, R.layout.list_item, android.R.id.text1, objects);
            files = objects;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) getContext()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                row = inflater.inflate(R.layout.list_item, parent, false);
            } else {
                row = convertView;
            }
            File object = files.get(position);
            ImageView imageView = (ImageView) row.findViewById(R.id.file_picker_image);
            TextView textView = (TextView) row.findViewById(R.id.file_picker_text);
            textView.setSingleLine(true);
            textView.setText(object.getName());
            if (object.isFile()) {
                imageView.setImageResource(R.drawable.img_file);
            } else {
                imageView.setImageResource(R.drawable.img_folder);
            }
            return row;
        }
    }

    private class FileComparator implements Comparator<File> {
        public int compare(File f1, File f2) {
            if (f1 == f2) {
                return 0;
            }
            if (f1.isDirectory() && f2.isFile()) // Show directories above files
            {
                return -1;
            }
            if (f1.isFile() && f2.isDirectory()) // Show files below directories
            {
                return 1;
            }
            // Sort the directories alphabetically
            return f1.getName().compareToIgnoreCase(f2.getName());
        }
    }

}

